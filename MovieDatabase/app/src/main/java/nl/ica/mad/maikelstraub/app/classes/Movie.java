package nl.ica.mad.maikelstraub.app.classes;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

public class Movie implements Serializable, Parcelable {
    private Integer id;
    private String originalTitle;
    private String posterPath;
    private String backdropPath;
    private String overview;
    private Double voteAverage;
    private String releaseDate;

    public Movie() {

    }

    public void setId(Integer id) {
    this.id = id;
}
    public void setOriginalTitle(String title){
        this.originalTitle = title;
    }
    public void setPosterPath(String posterPath){
        this.posterPath = posterPath;
    }
    public void setBackdropPath(String backdropPath){
        this.backdropPath = backdropPath;
    }
    public void setOverview(String overview){
        this.overview = overview;
    }
    public void setVoteAverage(Double voteAvarage){
        this.voteAverage = voteAvarage;
    }
    public void setReleaseDate(String releaseDate){
        this.releaseDate = releaseDate;
    }

    public Integer getId() {
        return id;
    }
    public String getOriginalTitle(){
        return originalTitle;
    }
    public String getPosterPath(){
        return posterPath;
    }
    public String getBackdropPath(){
        return backdropPath;
    }
    public String getOverview(){
        return overview;
    }
    public Double getVoteAverage(){
        return voteAverage;
    }
    public String getReleaseDate(){
        return releaseDate;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.id);
        parcel.writeString(this.originalTitle);
        parcel.writeString(this.posterPath);
        parcel.writeString(this.backdropPath);
        parcel.writeString(this.overview);
        parcel.writeDouble(this.voteAverage);
        parcel.writeString(this.releaseDate);
    }

    public Movie(Parcel in) {
        this.id = in.readInt();
        this.originalTitle = in.readString();
        this.posterPath = in.readString();
        this.backdropPath = in.readString();
        this.overview = in.readString();
        this.voteAverage = in.readDouble();
        this.releaseDate = in.readString();
    }

    public static final Creator<Movie> CREATOR = new Creator<Movie>() {
        public Movie createFromParcel(Parcel source) {return new Movie(source);}

        public Movie[] newArray(int size) {return new Movie[size];}
    };
}