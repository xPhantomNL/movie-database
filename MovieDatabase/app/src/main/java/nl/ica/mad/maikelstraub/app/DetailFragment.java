/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nl.ica.mad.maikelstraub.app;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import nl.ica.mad.maikelstraub.app.adapters.ReviewAdapter;
import nl.ica.mad.maikelstraub.app.adapters.TrailerAdapter;
import nl.ica.mad.maikelstraub.app.classes.Movie;
import nl.ica.mad.maikelstraub.app.classes.Review;
import nl.ica.mad.maikelstraub.app.classes.Trailer;
import nl.ica.mad.maikelstraub.app.data.MovieDbHelper;
import nl.ica.mad.maikelstraub.app.data.MovieContract.MovieEntry;
import com.squareup.picasso.Picasso;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class DetailFragment extends Fragment {

    private static final String LOG_TAG = DetailFragment.class.getSimpleName();
    private Movie selectedMovie = null;
    private Trailer[] trailers;
    private Review[] reviews;
    private TrailerAdapter trailerAdapter;
    private ReviewAdapter reviewAdapter;

    public DetailFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_detail, container, false);
        GridView trailerView = (GridView) rootView.findViewById(R.id.grid_view_trailers);
        ListView reviewView = (ListView) rootView.findViewById(R.id.review_list);

        Bundle arguments = getArguments();
        if (arguments != null) {
            selectedMovie = (Movie) arguments.getSerializable("Movie");
        }

        if(selectedMovie != null) {
            trailerAdapter = new TrailerAdapter(getActivity(), new ArrayList<Trailer>());
            trailerView.setAdapter(trailerAdapter);

            reviewAdapter = new ReviewAdapter(getActivity(), new ArrayList<Review>());
            reviewView.setAdapter(reviewAdapter);

            Picasso.with(getActivity()).load(selectedMovie.getBackdropPath()).placeholder(R.drawable.backdrop_placeholder).into((ImageView) rootView.findViewById(R.id.poster));
            ((TextView) rootView.findViewById(R.id.rating_text)).setText("Avg score: " + selectedMovie.getVoteAverage().toString().substring(0, 3) + " / 10");

            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            try {
                String date = DateUtils.formatDateTime(getActivity(),
                        formatter.parse(selectedMovie.getReleaseDate()).getTime(), DateUtils.FORMAT_SHOW_DATE | DateUtils.FORMAT_SHOW_YEAR);
                ((TextView) rootView.findViewById(R.id.release_text)).setText("Released: " + date);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            ((TextView) rootView.findViewById(R.id.detail_text)).setText(selectedMovie.getOverview());
            getActivity().setTitle(selectedMovie.getOriginalTitle());
        }

        final Button button = (Button) rootView.findViewById(R.id.add_to_favorite);
        if(selectedMovie != null) {
            changeFavoriteButton(itemExistsInDatabase(selectedMovie.getId()), button);
        }

        trailerView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Trailer trailer = (Trailer) trailerAdapter.getItem(position);

                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("http://www.youtube.com/watch?v=" + trailer.getSource()));
                startActivity(intent);
            }
        });

        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                MovieDbHelper dbHelper = new MovieDbHelper(getActivity().getApplicationContext());
                SQLiteDatabase sqLiteDatabase = dbHelper.getWritableDatabase();

                Boolean movieIsFavorited = itemExistsInDatabase(selectedMovie.getId());

                if(movieIsFavorited) {
                    Toast.makeText(getActivity(), R.string.toast_remove_favorite, Toast.LENGTH_SHORT).show();

                    sqLiteDatabase.delete(MovieEntry.TABLE_NAME, MovieEntry.COLUMN_MOVIE_ID + " = "  + selectedMovie.getId(), null);
                    sqLiteDatabase.close();

                    changeFavoriteButton(false, button);
                } else {
                    Toast.makeText(getActivity(), R.string.toast_add_favorite, Toast.LENGTH_SHORT).show();

                    ContentValues values = new ContentValues();
                    values.put(MovieEntry.COLUMN_MOVIE_ID, selectedMovie.getId());
                    values.put(MovieEntry.COLUMN_MOVIE_TITLE, selectedMovie.getOriginalTitle());
                    values.put(MovieEntry.COLUMN_MOVIE_POSTER, selectedMovie.getPosterPath());
                    values.put(MovieEntry.COLUMN_MOVIE_BACKDROP, selectedMovie.getBackdropPath());
                    values.put(MovieEntry.COLUMN_MOVIE_OVERVIEW, selectedMovie.getOverview());
                    values.put(MovieEntry.COLUMN_MOVIE_RATING, selectedMovie.getVoteAverage());
                    values.put(MovieEntry.COLUMN_MOVIE_RELEASE_DATE, selectedMovie.getReleaseDate());

                    sqLiteDatabase.insert(MovieEntry.TABLE_NAME, null, values);
                    sqLiteDatabase.close();

                    changeFavoriteButton(true, button);
                }
            }
        });

        return rootView;
    }

    public void changeFavoriteButton(Boolean isFavorite, Button button) {
        if(isFavorite) {
            button.setTextColor(Color.parseColor("#008000"));
            button.setText(R.string.button_favorited);
        } else {
            button.setTextColor(Color.parseColor("#282828"));
            button.setText(R.string.button_favorite);
        }
    }

    public boolean itemExistsInDatabase(Integer movieId) {
        MovieDbHelper dbHelper = new MovieDbHelper(getActivity().getApplicationContext());
        SQLiteDatabase sqLiteDatabase = dbHelper.getWritableDatabase();

        Cursor cursor = sqLiteDatabase.rawQuery("Select * from " + MovieEntry.TABLE_NAME + " where " + MovieEntry.COLUMN_MOVIE_ID + " = " + movieId, null);

        if(cursor.getCount() <= 0){
            cursor.close();
            return false;
        } else {
            cursor.close();
            return true;
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();

        if(selectedMovie != null) {
            FetchTrailerTask trailerTask = new FetchTrailerTask();
            trailerTask.execute();

            FetchReviewTask reviewTask = new FetchReviewTask();
            reviewTask.execute();
        }
    }

    public class FetchTrailerTask extends AsyncTask<String, Void, Trailer[]> {
        //Code die we kregen met de sunshine app.
        @Override
        protected Trailer[] doInBackground(String... params) {
            HttpURLConnection urlConnection = null;
            BufferedReader reader = null;
            String trailersJsonStr = null;

            try {
                String baseUrl = "https://api.themoviedb.org/3/movie/"+selectedMovie.getId()+"/trailers";
                String apiKey = "?api_key=" + BuildConfig.MOVIE_DB_API_KEY;
                URL url = new URL(baseUrl.concat(apiKey));

                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("GET");
                urlConnection.connect();

                InputStream inputStream = urlConnection.getInputStream();
                StringBuffer buffer = new StringBuffer();
                if (inputStream == null) {
                    return null;
                }
                reader = new BufferedReader(new InputStreamReader(inputStream));

                String line;
                while ((line = reader.readLine()) != null) {
                    buffer.append(line + "\n");
                }

                if (buffer.length() == 0) {
                    return null;
                }
                trailersJsonStr = buffer.toString();
            } catch (IOException e) {
                Log.e("MainActivityFragment", "Error ", e);
                return null;
            } finally {
                if (urlConnection != null) {
                    urlConnection.disconnect();
                }
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (final IOException e) {
                        Log.e("MainActivityFragment", "Error closing stream", e);
                    }
                }
            }
            try {
                return parseTrailerJson(trailersJsonStr);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Trailer[] trailerList) {
            trailers = trailerList;

            if (trailerList != null) {
                trailerAdapter.clear();
                for (Trailer trailer : trailerList) {
                    trailerAdapter.add(trailer);
                }
            }
        }

        private Trailer[] parseTrailerJson(String trailerJsonStr) throws JSONException {
            // JSON tags
            final String TRAILER_RESULTS = "youtube";
            final String TRAILER_SOURCE = "source";
            final String TRAILER_NAME = "name";
            final String TRAILER_TYPE = "type";

            JSONObject moviesJson = new JSONObject(trailerJsonStr);
            JSONArray trailersArray = moviesJson.getJSONArray(TRAILER_RESULTS);

            Trailer[] trailerList = new Trailer[trailersArray.length()];

            for (int i = 0; i < trailersArray.length(); i++) {
                JSONObject trailerObj = trailersArray.getJSONObject(i);

                Trailer trailer = new Trailer();
                trailer.setName(trailerObj.getString(TRAILER_NAME));
                trailer.setType(trailerObj.getString(TRAILER_TYPE));
                trailer.setSource(trailerObj.getString(TRAILER_SOURCE));

                trailerList[i] = trailer;
            }

            return trailerList;
        }
    }

    public class FetchReviewTask extends AsyncTask<String, Void, Review[]> {
        //Code die we kregen met de sunshine app.
        @Override
        protected Review[] doInBackground(String... params) {
            HttpURLConnection urlConnection = null;
            BufferedReader reader = null;
            String reviewsJsonStr = null;

            try {
                String baseUrl = "https://api.themoviedb.org/3/movie/"+selectedMovie.getId()+"/reviews";
                String apiKey = "?api_key=" + BuildConfig.MOVIE_DB_API_KEY;
                URL url = new URL(baseUrl.concat(apiKey));

                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("GET");
                urlConnection.connect();

                InputStream inputStream = urlConnection.getInputStream();
                StringBuffer buffer = new StringBuffer();
                if (inputStream == null) {
                    return null;
                }
                reader = new BufferedReader(new InputStreamReader(inputStream));

                String line;
                while ((line = reader.readLine()) != null) {
                    buffer.append(line + "\n");
                }

                if (buffer.length() == 0) {
                    return null;
                }
                reviewsJsonStr = buffer.toString();
            } catch (IOException e) {
                Log.e("MainActivityFragment", "Error ", e);
                return null;
            } finally {
                if (urlConnection != null) {
                    urlConnection.disconnect();
                }
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (final IOException e) {
                        Log.e("MainActivityFragment", "Error closing stream", e);
                    }
                }
            }
            try {
                return parseReviewJson(reviewsJsonStr);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Review[] reviewList) {
            reviews = reviewList;

            if (reviewList.length > 0) {
                reviewAdapter.clear();
                for (Review review : reviewList) {
                    reviewAdapter.add(review);
                }
            } else {
                reviewAdapter.clear();

                Review r = new Review();
                r.setId(0);
                r.setAuthor("No reviews for this movie");
                r.setContent("We couldn't find any reviews for this movie.");

                reviewAdapter.add(r);
            }
        }

        private Review[] parseReviewJson(String reviewJsonStr) throws JSONException {
            // JSON tags
            final String REVIEW_RESULTS = "results";
            final String REVIEW_AUTHOR = "author";
            final String REVIEW_CONTENT = "content";

            JSONObject reviewJson = new JSONObject(reviewJsonStr);
            JSONArray reviewsArray = reviewJson.getJSONArray(REVIEW_RESULTS);

            Review[] reviewList = new Review[reviewsArray.length()];

            for (int i = 0; i < reviewsArray.length(); i++) {
                JSONObject reviewObj = reviewsArray.getJSONObject(i);

                Review review = new Review();
                review.setAuthor(reviewObj.getString(REVIEW_AUTHOR));
                review.setContent(reviewObj.getString(REVIEW_CONTENT));

                reviewList[i] = review;
            }

            return reviewList;
        }
    }
}