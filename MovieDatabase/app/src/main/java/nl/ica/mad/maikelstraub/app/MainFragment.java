/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nl.ica.mad.maikelstraub.app;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.Toast;

import nl.ica.mad.maikelstraub.app.adapters.ImageListAdapter;
import nl.ica.mad.maikelstraub.app.classes.Movie;
import nl.ica.mad.maikelstraub.app.data.MovieDbHelper;
import nl.ica.mad.maikelstraub.app.data.MovieContract;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class MainFragment extends Fragment {
    private ImageListAdapter mListAdapter;
    private Movie[] moviesList;

    private int mPosition = ListView.INVALID_POSITION;
    private static final String SELECTED_KEY = "selected_position";

    /**
     * A callback interface that all activities containing this fragment must
     * implement. This mechanism allows activities to be notified of item
     * selections.
     */
    public interface Callback {
        /**
         * DetailFragmentCallback for when an item has been selected.
         */
        public void onItemSelected(Movie movie);
    }

    public MainFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Add this line in order for this fragment to handle menu events.
        setHasOptionsMenu(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        FetchMovieTask movieTask = new FetchMovieTask(getActivity());

        if (id == R.id.action_sortPopular) {
            Toast.makeText(getActivity(), R.string.toast_sort_popular, Toast.LENGTH_SHORT).show();
            changeSortingMode("popular");

            movieTask.execute("");
        } else if (id == R.id.action_sortRating) {
            Toast.makeText(getActivity(), R.string.toast_sort_toprated, Toast.LENGTH_SHORT).show();
            changeSortingMode("top_rated");

            movieTask.execute("");
        } else if (id == R.id.action_showFavorites) {
            FetchFavorites();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mListAdapter = new ImageListAdapter(getActivity(), new ArrayList<String>());

        View rootView = inflater.inflate(R.layout.fragment_main, container, false);

        GridView gridView = (GridView) rootView.findViewById(R.id.grid_view_movies);
        gridView.setAdapter(mListAdapter);

        // We'll call our MainActivity
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                String movieUrl = (String) mListAdapter.getItem(position);
                Movie movie = findMovieFromList(movieUrl);

                ((Callback) getActivity()).onItemSelected(movie);
            }
        });

        if (savedInstanceState != null && savedInstanceState.containsKey(SELECTED_KEY)) {
            mPosition = savedInstanceState.getInt(SELECTED_KEY);
        }

        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();

        FetchMovieTask movieTask = new FetchMovieTask(getActivity());
        movieTask.execute("");
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        if (mPosition != ListView.INVALID_POSITION) {
            outState.putInt(SELECTED_KEY, mPosition);
        }
        super.onSaveInstanceState(outState);
    }

    private void changeSortingMode(String sortMode) {
        SharedPreferences sharedPref = getActivity().getPreferences(getActivity().MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString("SortMode", sortMode);
        editor.commit();
    }

    public Movie findMovieFromList(String moviePosterUrl) {
        for(Movie m : moviesList) {
            if (m.getPosterPath().equals(moviePosterUrl)) {
                return m;
            }
        }

        return null;
    }

    public class FetchMovieTask extends AsyncTask<String, Void, Movie[]> {
        private final String LOG_TAG = "FetchMovieTask";
        private final Context mContext;

        public FetchMovieTask(Context context) {
            mContext = context;
        }

        private Movie[] getMovieDataFromJson(String moviesJsonStr) throws JSONException {
            // JSON tags
            final String MOVIE_RESULTS = "results";
            final String MOVIE_ID = "id";
            final String MOVIE_ORIGINAL_TITLE = "original_title";
            final String MOVIE_POSTER_PATH = "poster_path";
            final String MOVIE_BACKDROP_PATH = "backdrop_path";
            final String MOVIE_OVERVIEW = "overview";
            final String MOVIE_VOTE_AVERAGE = "vote_average";
            final String MOVIE_RELEASE_DATE = "release_date";

            final String MOVIE_POSTER_BASE_URL = "http://image.tmdb.org/t/p/";
            final String MOVIE_POSTER_SIZE = "w342";
            final String MOVIE_BACKDROP_SIZE = "w780";

            JSONObject moviesJson = new JSONObject(moviesJsonStr);
            JSONArray resultsArray = moviesJson.getJSONArray(MOVIE_RESULTS);

            // Create array of Movie objects that stores data from the JSON string
            Movie[] movieList = new Movie[resultsArray.length()];

            // Traverse through movies one by one and get data
            for (int i = 0; i < resultsArray.length(); i++) {
                // Initialize each object before it can be used
                movieList[i] = new Movie();

                // Object contains all tags we're looking for
                JSONObject movieInfo = resultsArray.getJSONObject(i);
                String fullPosterPath = MOVIE_POSTER_BASE_URL + MOVIE_POSTER_SIZE + movieInfo.getString(MOVIE_POSTER_PATH);
                String fullBackdropPath = MOVIE_POSTER_BASE_URL + MOVIE_BACKDROP_SIZE + movieInfo.getString(MOVIE_BACKDROP_PATH);

                // Store data in movie object
                movieList[i].setId((movieInfo.getInt(MOVIE_ID)));
                movieList[i].setOriginalTitle(movieInfo.getString(MOVIE_ORIGINAL_TITLE));
                movieList[i].setPosterPath(fullPosterPath);
                movieList[i].setBackdropPath(fullBackdropPath);
                movieList[i].setOverview(movieInfo.getString(MOVIE_OVERVIEW));
                movieList[i].setVoteAverage(movieInfo.getDouble(MOVIE_VOTE_AVERAGE));
                movieList[i].setReleaseDate(movieInfo.getString(MOVIE_RELEASE_DATE));
            }
            return movieList;
        }

        @Override
        protected Movie[] doInBackground(String... params) {
            // These two need to be declared outside the try/catch
            // so that they can be closed in the finally block.
            HttpURLConnection urlConnection = null;
            BufferedReader reader = null;

            // Will contain the raw JSON response as a string.
            String moviesJsonStr = null;

            try {
                SharedPreferences sharedPref = getActivity().getPreferences(Context.MODE_PRIVATE);
                String sortMode = sharedPref.getString("SortMode", "popular");

                String baseUrl = "https://api.themoviedb.org/3/movie/"+sortMode;
                String apiKey = "?api_key=" + BuildConfig.MOVIE_DB_API_KEY;
                URL url = new URL(baseUrl.concat(apiKey));

                // Create the request to OpenWeatherMap, and open the connection
                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("GET");
                urlConnection.connect();

                // Read the input stream into a String
                InputStream inputStream = urlConnection.getInputStream();
                StringBuffer buffer = new StringBuffer();
                if (inputStream == null) {
                    // Nothing to do.
                    return null;
                }
                reader = new BufferedReader(new InputStreamReader(inputStream));

                String line;
                while ((line = reader.readLine()) != null) {
                    // Since it's JSON, adding a newline isn't necessary (it won't affect parsing)
                    // But it does make debugging a *lot* easier if you print out the completed
                    // buffer for debugging.
                    buffer.append(line + "\n");
                }

                if (buffer.length() == 0) {
                    // Stream was empty.  No point in parsing.
                    return null;
                }
                moviesJsonStr = buffer.toString();
                return getMovieDataFromJson(moviesJsonStr);
            } catch (IOException e) {
                Log.e(LOG_TAG, "Error ", e);
                // If the code didn't successfully get the weather data, there's no point in attempting
                // to parse it.
            } catch (JSONException e) {
                Log.e(LOG_TAG, e.getMessage(), e);
                e.printStackTrace();
            } finally {
                if (urlConnection != null) {
                    urlConnection.disconnect();
                }
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (final IOException e) {
                        Log.e(LOG_TAG, "Error closing stream", e);
                    }
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(Movie[] movies) {
            super.onPostExecute(movies);
            if (movies != null) {
                moviesList = movies;

                mListAdapter.clear();
                for(Movie m : movies) {
                    mListAdapter.add(m.getPosterPath());
                }
            }
        }
    }

    public void FetchFavorites() {
        MovieDbHelper dbHelper = new MovieDbHelper(getActivity().getApplicationContext());
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        mListAdapter.clear();

        String[] columns = new String[] {
                MovieContract.MovieEntry.COLUMN_MOVIE_ID,
                MovieContract.MovieEntry.COLUMN_MOVIE_TITLE,
                MovieContract.MovieEntry.COLUMN_MOVIE_POSTER,
                MovieContract.MovieEntry.COLUMN_MOVIE_BACKDROP,
                MovieContract.MovieEntry.COLUMN_MOVIE_OVERVIEW,
                MovieContract.MovieEntry.COLUMN_MOVIE_RATING,
                MovieContract.MovieEntry.COLUMN_MOVIE_RELEASE_DATE};

        Cursor c = db.query(MovieContract.MovieEntry.TABLE_NAME, columns, null, null, null, null, null);
        if(c.getCount() <= 0) {
            Toast.makeText(getActivity(), R.string.toast_show_favorites_empty, Toast.LENGTH_SHORT).show();
            c.close();

            FetchMovieTask movieTask = new FetchMovieTask(getActivity());
            movieTask.execute("");
        } else {
            Toast.makeText(getActivity(), R.string.toast_show_favorites, Toast.LENGTH_SHORT).show();
            Integer i = 0;

            if(moviesList == null) {
                moviesList = new Movie[c.getCount()];
            }

            while (c.moveToNext()) {
                Movie m = new Movie();

                m.setId(c.getInt(0));
                m.setOriginalTitle(c.getString(1));
                m.setPosterPath(c.getString(2));
                m.setBackdropPath(c.getString(3));
                m.setOverview(c.getString(4));
                m.setVoteAverage(c.getDouble(5));
                m.setReleaseDate(c.getString(6));

                if(moviesList[0] == null) {
                    moviesList[i] = m;
                }
                i++;

                mListAdapter.add(c.getString(2));
            }

            c.close();
        }
    }
}