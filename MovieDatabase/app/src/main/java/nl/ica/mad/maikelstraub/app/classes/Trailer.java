package nl.ica.mad.maikelstraub.app.classes;

public class Trailer {
    private String source;
    private String name;
    private String type;

    public void setSource(String source){
        this.source = source;
    }
    public void setName(String name){
        this.name = name;
    }
    public void setType(String type){
        this.type = type;
    }

    public String getSource(){
        return source;
    }
    public String getName(){
        return name;
    }
    public String getType(){
        return type;
    }

}
