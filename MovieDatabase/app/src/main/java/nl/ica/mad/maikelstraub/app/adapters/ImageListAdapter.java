package nl.ica.mad.maikelstraub.app.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import nl.ica.mad.maikelstraub.app.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;


public class ImageListAdapter extends ArrayAdapter {
    private Context context;
    private LayoutInflater inflater;

    private ArrayList<String> imageUrls;

    public ImageListAdapter(Context context, ArrayList<String> imageUrls) {
        super(context, R.layout.poster_grid_item, imageUrls);

        this.context = context;
        this.imageUrls = imageUrls;

        inflater = LayoutInflater.from(context);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (null == convertView) {
            convertView = inflater.inflate(R.layout.poster_grid_item, parent, false);
        }

        if(imageUrls.get(position).contains("http://image.tmdb.org/t/p/")) {
            Picasso.with(context).load(imageUrls.get(position)).into((ImageView) convertView);
        } else {
            Picasso.with(context).load("http://image.tmdb.org/t/p/w376/" + imageUrls.get(position)).into((ImageView) convertView);
        }

        return convertView;
    }
}