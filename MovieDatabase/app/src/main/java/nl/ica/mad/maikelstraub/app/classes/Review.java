package nl.ica.mad.maikelstraub.app.classes;

public class Review {
    private Integer id;
    private String author;
    private String content;

    public void setId(Integer id) {
        this.id = id;
    }
    public void setAuthor(String author){
        this.author = author;
    }
    public void setContent(String content){
        this.content = content;
    }

    public Integer getId() {
        return id;
    }
    public String getAuthor(){
        return author;
    }
    public String getContent(){
        return content;
    }
}