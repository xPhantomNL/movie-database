package nl.ica.mad.maikelstraub.app.data;

import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.content.Context;
import nl.ica.mad.maikelstraub.app.data.MovieContract.MovieEntry;

public class MovieDbHelper extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 2;
    static final String DATABASE_NAME = "movie.db";

    public MovieDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        System.out.println("ONCREATE MOVIEDBHELPER");
        final String SQL_MOVIE_CREATE_TABLE = "CREATE TABLE " + MovieEntry.TABLE_NAME + " (" +
                MovieEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                MovieEntry.COLUMN_MOVIE_ID + " INTEGER NOT NULL, " +
                MovieEntry.COLUMN_MOVIE_TITLE + " STRING NOT NULL, " +
                MovieEntry.COLUMN_MOVIE_POSTER + " STRING NOT NULL, " +
                MovieEntry.COLUMN_MOVIE_BACKDROP + " STRING NOT NULL, " +
                MovieEntry.COLUMN_MOVIE_OVERVIEW + " STRING NOT NULL, " +
                MovieEntry.COLUMN_MOVIE_RATING + " STRING NOT NULL, " +
                MovieEntry.COLUMN_MOVIE_RELEASE_DATE + " STRING NOT NULL)";

        sqLiteDatabase.execSQL(SQL_MOVIE_CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + MovieContract.MovieEntry.TABLE_NAME);
        onCreate(sqLiteDatabase);
    }
}
