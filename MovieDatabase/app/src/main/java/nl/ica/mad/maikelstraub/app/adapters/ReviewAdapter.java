package nl.ica.mad.maikelstraub.app.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import java.util.ArrayList;

import nl.ica.mad.maikelstraub.app.R;
import nl.ica.mad.maikelstraub.app.classes.Review;

public class ReviewAdapter extends ArrayAdapter {
    private Context context;
    private LayoutInflater inflater;

    private ArrayList<Review> reviews;

    public ReviewAdapter(Context context, ArrayList<Review> reviews) {
        super(context, R.layout.review_item, reviews);

        this.context = context;
        this.reviews = reviews;

        inflater = LayoutInflater.from(context);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (null == convertView) {
            convertView = inflater.inflate(R.layout.review_item, parent, false);
        }

        Review review = reviews.get(position);

        ((TextView) convertView.findViewById(R.id.review_author)).setText(review.getAuthor());
        ((TextView) convertView.findViewById(R.id.review_content)).setText(review.getContent());

        return convertView;
    }
}