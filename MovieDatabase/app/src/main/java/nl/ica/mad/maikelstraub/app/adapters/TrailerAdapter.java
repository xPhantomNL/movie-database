package nl.ica.mad.maikelstraub.app.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import nl.ica.mad.maikelstraub.app.R;
import nl.ica.mad.maikelstraub.app.classes.Trailer;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;


public class TrailerAdapter extends ArrayAdapter {
    private Context context;
    private LayoutInflater inflater;

    private ArrayList<Trailer> trailers;

    public TrailerAdapter(Context context, ArrayList<Trailer> trailers) {
        super(context, R.layout.trailer_grid_item, trailers);

        this.context = context;
        this.trailers = trailers;

        inflater = LayoutInflater.from(context);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        if (null == convertView) {
            convertView = inflater.inflate(R.layout.trailer_grid_item, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        }

        Trailer trailer = trailers.get(position);

        viewHolder = (ViewHolder) convertView.getTag();
        viewHolder.titleView.setText(trailer.getType());

        Picasso.with(context).load("http://img.youtube.com/vi/" +trailer.getSource()+"/0.jpg").placeholder(R.drawable.trailer_placeholder).into(viewHolder.imageView);

        return convertView;
    }

    public static class ViewHolder {
        public final ImageView imageView;
        public final TextView titleView;

        public ViewHolder(View view) {
            imageView = (ImageView) view.findViewById(R.id.trailer_image);
            titleView = (TextView) view.findViewById(R.id.trailer_name);
        }
    }
}